import { verify } from 'jsonwebtoken';

const APP_SECRET = 'appsecret321';

const getUserId = request => {
    const Authorization = request.get('Authorization');
    if (Authorization) {
        const token = Authorization.replace('Bearer ', '');
        const verifiedToken = verify(token, APP_SECRET);
        return verifiedToken && verifiedToken.userId;
    }
};

const throwError = (condition, message) => {
    if (condition) throw new Error(message);
};

module.exports = {
    APP_SECRET,
    getUserId,
    throwError,
};