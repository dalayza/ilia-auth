import moment from 'moment';
import { hash } from 'bcrypt';

import { APP_SECRET } from '../utils';

import TokenLink from '../models/TokenLink';

import { findUserByEmail, findUserById } from './User';

export const findTokenLinkByTokenString = token => TokenLink.findOne({ token }).exec();

export const findTokenByIdAndDelete = _id => TokenLink.findByIdAndRemove({ _id }).exec();

export const removeUserTokens = userId => TokenLink.deleteMany({ userId }).exec();

export const create = (values, logger) => (
  new Promise((resolve, reject) => {
    const token = new TokenLink(values);
    token.save(err => {
      if (err) {
        logger.error(err);
        return reject(err);
      }
      logger.info(`TokenLink created on MongoDB: ${token.token}`);
      resolve(token);
    });
  })
);

export const generatePasswordRecoveryToken = async (userEmail, logger) => (
  new Promise(async (resolve, reject) => {
    const user = await findUserByEmail(userEmail);
    if (!user) { return reject(); }
    await removeUserTokens(user._id);

    const currentTimestamp = moment();
    const tokenExpirationTimestamp = moment().add(1, 'hours');
    const tokenComposition = `${APP_SECRET}_${user.email}_${currentTimestamp.format()}`;
    const hashedToken = await hash(tokenComposition, 10);

    const token = await create({
      userId: user._id,
      token: hashedToken,
      expiresAt: tokenExpirationTimestamp
    }, logger);
    resolve(token);
  })
);

export const validatePasswordRecoveryToken = (tokenStr, logger) => (
  new Promise(async (resolve, reject) => {
    const token = await findTokenLinkByTokenString(tokenStr);
    if (!token) { return reject('Token inexistente.'); }
    if (moment().isAfter(token.expiresAt)) { return reject('Token expirado.'); }

    const user = await findUserById(token.userId);
    if (!user) { return reject('Usuário inexistente.'); }
    resolve(token);
  })
);
