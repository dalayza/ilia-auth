import User from '../models/User';

export const findUserById = (_id) => User.findOne({ _id }).exec();

export const findUserByEmail = (email) => User.findOne({ email }).exec();

export const create = (values, logger) => (
    new Promise((resolve, reject) => {
        const user = new User(values);
        user.save(err => {
            if (err) {
                logger.error(err);
                return reject(err);
            }
            logger.info(`User created on MongoDB: ${user.email}`);
            resolve(user);
        });
    })
);

export const update = (_id, values, logger) => (
    new Promise((resolve, reject) => {
        User.findOneAndUpdate({ _id }, values, (err, doc) => {
            if (err) {
                logger.error(err);
                return reject(err);
            }
            logger.info(`Update user ${doc.name} on MongoDB`);
            resolve(doc);
        });
    })
);

export const updatePassword = (_id, password, logger) => (
    new Promise((resolve, reject) => {
        User.findOneAndUpdate({ _id }, { password }, (err, doc) => {
            if (err) {
                logger.error(err);
                return reject(err);
            }
            logger.info(`Update password user ${doc.name} on MongoDB`);
            resolve({});
        });
    })
);