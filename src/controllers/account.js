import axios from 'axios';
import { hash, compare } from 'bcrypt';
import { sign } from 'jsonwebtoken';
import getConfig from '../safe/config';
import { APP_SECRET, getUserId, throwError } from '../utils';
import DefaultResponse from '../response/default-response';
import {
    create as createUser,
    update as updateUser,
    updatePassword as savePassword,
    findUserByEmail,
    findUserById
} from './User';
import {
    generatePasswordRecoveryToken,
    validatePasswordRecoveryToken,
    findTokenByIdAndDelete as deleteToken
} from './TokenLink';

export const signin = async(values, logger) => {
    let user = await findUserByEmail(values.email);
    throwError(!!user, 'Este email já foi cadastrado!');
    const hashedPassword = await hash(values.password, 10);
    // TODO: Falta implementar regras de ROLE.
    user = await createUser({...values, password: hashedPassword, role: "ADMIN", approvedAt: new Date() }, logger);
    return new DefaultResponse({
        resource: 'signup',
        data: { name: user.name, email: user.email }
    });
};

export const login = async(req, res, logger) => {
    res.status(401);
    const { email, password } = req.body;
    const messageError = `Unauthorized, wrong email or password!`;
    const user = await findUserByEmail(email);
    throwError(!user, messageError);
    const passwordValid = await compare(password, user.password);
    throwError(!passwordValid, messageError);
    throwError(!user.approvedAt, 'Seu cadastro está pendente de aprovação.');
    return new DefaultResponse({
        resource: 'login',
        data: {
            token: sign({ userId: user.id }, APP_SECRET),
            user: {
                name: user.name,
                email: user.email,
                role: user.role,
                createAt: user.createAt
            }
        }
    });
};

export const updatePassword = async(req, res, logger) => {
    const userId = getUserId(req);
    let user = await findUserById(userId);
    throwError(!user, 'Unauthorised!');
    const passwordValid = await compare(req.body.password, user.password);
    throwError(!passwordValid, 'Invalid password!');
    const hashedPassword = await hash(req.body.newPassword, 10);
    await savePassword(userId, hashedPassword, logger);
    return new DefaultResponse({
        resource: 'updatePassword',
        data: true
    });
};

export const updateAccount = async(req, res, logger) => {
    res.status(403);
    const userId = getUserId(req);
    let user = await findUserById(userId);
    throwError(!user, 'Unauthorised!');
    const values = {
        name: req.body.name,
        email: req.body.email,
    };
    user = await updateUser(userId, values, logger);
    return new DefaultResponse({
        resource: 'updateAccount',
        data: {
            name: user.name,
            email: user.email,
            updatedAt: user.updatedAt
        }
    });
};

export const recoverPassword = async(req, logger) => {
    const { email } = req.body;
    throwError(!(email), 'O e-mail para a recuperação de senha deverá ser informado.');

    let token, user;
    try {
        token = await generatePasswordRecoveryToken(email, logger);
    } catch {
        return;
    }

    try {
        user = await findUserById(token.userId).then(userData => {
            if (!userData) { throw new Error(`User data with id "${token.userId}" were not found.`); }
            return userData;
        });
    } catch (err) {
        logger.error(err);
        await deleteToken(token._id);
        return;
    }

    try {
        const config = await getConfig('insurance-auth-service');
        if (!config) { throw new Error('insurance-auth-service config file was not found.'); }

        const passwordRecoveryURL = config.get('services.auth.password_recovery_url');
        if (!passwordRecoveryURL) { throw new Error('Password recovery url could not be found.'); }

        await axios.post('http://insurance-mail-service:11015/password_recovery', {
            recipient: user.email,
            username: user.name,
            recovery_url: `${passwordRecoveryURL}${encodeURIComponent(token.token)}`
        });
        return;
    } catch (err) {
        logger.error(err);
        throw new Error('Houve um erro ao enviar o e-mail de recuperação de senha.');
    }
};

export const validatePasswordResetToken = async(req, logger) => {
    const { token: tokenStr } = req.body;
    throwError((!tokenStr), 'Token não informado.');

    validatePasswordRecoveryToken(tokenStr, logger).then(token => (
        new DefaultResponse({
            resource: 'validateToken',
            data: {
                valid: !!(token)
            }
        })
    )).catch(err => {
        logger.error(err);
        throw new Error('Token inválido');
    });
}

export const resetPassword = async(req, logger) => {
    const { token: tokenStr, password } = req.body;
    throwError((!tokenStr), 'Token não informado.');
    throwError((!password), 'A nova senha não foi informada.');

    let token;
    try {
        token = await validatePasswordRecoveryToken(tokenStr, logger);
    } catch (err) {
        logger.error(err);
        throw new Error('Token inválido');
    }

    try {
        const hashedPassword = await hash(password, 10);

        const user = await updateUser(token.userId, { password: hashedPassword }, logger);
        await deleteToken(token._id);

        return new DefaultResponse({
            resource: 'resetPassword',
            data: {
                name: user.name,
                email: user.email,
                updatedAt: user.updatedAt
            }
        });
    } catch (err) {
        logger.error(err);
        throw new Error('Não foi possível atualizar a senha deste usuário.');
    }
};