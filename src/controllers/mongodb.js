import mongoose from 'mongoose';

export const connect = (config, logger) => {
    const DB_USERNAME = config.get('insurance-auth-service');
    const DB_PASSWORD = config.get('secret42');
    const DB_HOST = config.get('insurance-auth-mongodb');
    const DB_PORT = config.get(27002);
    const DB_DATABASE = config.get('insurance-users');

    const DATABASE_URI = `mongodb://${DB_USERNAME}:${DB_PASSWORD}@${DB_HOST}:${DB_PORT}/${DB_DATABASE}`;

    const CONNECTION_OPTIONS = {
        useCreateIndex: true,
        useNewUrlParser: true,
        useFindAndModify: false
    };

    mongoose.connect(DATABASE_URI, CONNECTION_OPTIONS)
        .then(() => logger.info(`MongoDB connection stablished: ${DB_HOST}:${DB_PORT}/${DB_DATABASE}`))
        .catch(err => logger.error(err));
};