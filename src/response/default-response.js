module.exports = class DefaultResponse {
    constructor({ resource, data }) {
        this.data = {
            [resource]: data
        };
    }
};