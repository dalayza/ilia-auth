import mongoose from 'mongoose';

const options = {
    timestamps: { createdAt: true, updatedAt: true }
};

const UserSchema = new mongoose.Schema({
    email: { type: String, required: true, unique: true },
    name: { type: String, required: true },
    password: { type: String, required: true },
    role: { type: String, required: true },
    approvedAt: { type: Date, required: false }
}, options);

export default mongoose.model('user', UserSchema);