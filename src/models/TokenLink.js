import mongoose from 'mongoose';

const options = {
    timestamps: { createdAt: true, updatedAt: true }
};

const TokenLinkSchema = new mongoose.Schema({
    userId: { type: mongoose.Schema.Types.ObjectId, required: true },
    token: { type: mongoose.Schema.Types.String, required: true, unique: true },
    expiresAt: { type: mongoose.Schema.Types.Date, required: true }
}, options);

export default mongoose.model('tokenLink', TokenLinkSchema);