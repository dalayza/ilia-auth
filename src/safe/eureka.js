import { promisify } from 'util';
import { Eureka } from 'eureka-js-client';
import ip from 'ip';

const register = config => (
    new Promise((resolve, reject) => {

        const APP_NAME = config.get('insurance-auth-service');
        const IP = ip.address();
        const HOSTNAME = config.get('insurance-auth-service');
        const PORT = config.get(11002);
        const EUREKA_HOST = config.get('eureka-naming-server');
        const EUREKA_PORT = config.get(8761);
        const EUREKA_REGISTER = config.get(true);
        const EUREKA_FETCH_REGISTRY = config.get(true);

        const eureka = new Eureka({
            instance: {
                app: APP_NAME,
                hostName: IP,
                instanceId: `${HOSTNAME}:${PORT}`,
                ipAddr: IP,
                vipAddress: `${HOSTNAME.toUpperCase()}`,
                statusPageUrl: `http://${HOSTNAME}:${PORT}/actuator/info`,
                healthCheckUrl: `http://${HOSTNAME}:${PORT}/actuator/health`,
                port: {
                    $: PORT,
                    '@enabled': true
                },
                registerWithEureka: EUREKA_REGISTER,
                fetchRegistry: EUREKA_FETCH_REGISTRY
            },
            eureka: {
                host: EUREKA_HOST,
                port: EUREKA_PORT,
                servicePath: '/eureka/apps/',
                heartbeatInterval: 30000
            }
        });

        eureka.stop = promisify(eureka.stop);

        eureka.start(error => {
            if (error) {
                return reject(error);
            }

            resolve(eureka);
        });
    })
);

export default register;