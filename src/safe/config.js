import client from 'cloud-config-client';
import fs from 'fs';
import path from 'path';
import YAML from 'yaml';
import InsuranceAuthService from '../config/insurance-auth-service.yml';

const getConfig = async application => {
    const filePath = path.join(__dirname, InsuranceAuthService);
    const file = fs.readFileSync(filePath, 'utf8');
    const cloudConfigUri = YAML.parse(file);

    return client.load({
            endpoint: cloudConfigUri,
            application,
        })
        .then(config => config)
        .catch(() => null);
};

export default getConfig;