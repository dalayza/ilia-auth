import axios from 'axios';
import bunyan from 'bunyan';

let LOGSTASH_URL = null;

// Override console

console.authorizedOutput = console.log;
console.log = msg => logstashLogger.trace(msg);
console.trace = msg => logstashLogger.trace(msg);
console.debug = msg => logstashLogger.trace(msg);
console.info = msg => logstashLogger.trace(msg);
console.warn = msg => logstashLogger.trace(msg);
console.error = msg => logstashLogger.trace(msg);

// Streams

function ConsoleStream() {}
ConsoleStream.prototype.write = data => {
  console.authorizedOutput(`${formatDateTime(data.time)} ${mapLevelToName(data.level)} ${data.msg}`);
};

function LogstashStream() {}
LogstashStream.prototype.write = data => {
  if (!LOGSTASH_URL) {
    return;
  }

  const message = { ...data, level: mapLevelToName(data.level) };
  
  axios.post(LOGSTASH_URL, message)
    .catch(error => fileLogger.error(`Logstash error: ${error}`));
};

// Config

const consoleStreamConfig = {
  level: 'trace',
  type: 'raw',
  stream: new ConsoleStream()
};

const fileStreamConfig = {
  level: 'debug',
  type: 'rotating-file',
  path: './log/app.log',
  period: '1d',
  count: 3
};

const logstashStreamConfig = {
  level: 'info',
  type: 'raw',
  stream: new LogstashStream()
};

// Loggers

const fileLogger = bunyan.createLogger({
  name: 'insurance-auth-service',
  streams: [
    consoleStreamConfig,
    fileStreamConfig
  ]
});

const logstashLogger = bunyan.createLogger({
  name: 'insurance-auth-service',
  streams: [
    consoleStreamConfig,
    fileStreamConfig,
    logstashStreamConfig
  ]
});

const createLogger = logstash => {
  if (logstash) {
    LOGSTASH_URL = `http://logstash:${logstash.port.$}`; // `http://${logstash.hostName}:${logstash.port.$}`
    console.authorizedOutput(`Logstash configured to ${LOGSTASH_URL}`);
  }

  return logstashLogger;
};

export default createLogger;

// Utils

const formatDateTime = time => {
  const year = time.getFullYear();
  const month = fillLeftZeros(time.getMonth() + 1, 2);
  const day = fillLeftZeros(time.getDate(), 2);
  const hour = fillLeftZeros(time.getHours(), 2);
  const minutes = fillLeftZeros(time.getMinutes(), 2);
  const seconds = fillLeftZeros(time.getSeconds(), 2);
  const milliseconds = fillLeftZeros(time.getMilliseconds(), 3);

  return `${day}-${month}-${year} ${hour}:${minutes}:${seconds}.${milliseconds}`;
};

const fillLeftZeros = (number, length = 1) => {
  let filledNumber = number;

  for (let limit = Math.pow(10, length - 1); limit >= 10; limit /= 10) {
    if (number < limit) {
      filledNumber = `0${filledNumber}`;
    }
  }

  return filledNumber;
};

const mapLevelToName = level => {
  switch (level) {
    case bunyan.TRACE: return 'TRACE';
    case bunyan.DEBUG: return 'DEBUG';
    case bunyan.INFO: return 'INFO';
    case bunyan.WARN: return 'WARN';
    case bunyan.ERROR: return 'ERROR';
    case bunyan.FATAL: return 'FATAL';
    default: return level;
  }
};