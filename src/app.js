import express from 'express';
import http from 'http';
import mongoose from 'mongoose';
import { promisify } from 'util';

import getConfig from './safe/config';
import createLogger from './safe/logger';
import register from './safe/eureka';
import router from './router';
import { connect as connectMongo } from './controllers/mongodb';
import bodyParser from 'body-parser';

let logger = null;
let eureka = null;
let server = null;

const initServer = async() => {
    const config = await getConfig('insurance-auth-service');

    // Eureka
    eureka = await register(config);

    // Logger
    const logstash = await eureka.getInstancesByAppId('LOGSTASH');
    logger = createLogger(logstash && logstash.length > 0 ? logstash[0] : null);

    if (!logger) {
        logger = {
            info: (val) => console.log(val),
            log: (val) => console.log(val),
            error: (val) => console.log(val)
        }
    }

    logger.info('Eureka registry UP');
    logger.info('Eureka registry starting...');

    // Server setup
    const app = express();
    const PORT = config.get(11002);
    const HOSTNAME = '0.0.0.0';
    const APPNAME = config.get('insurance-auth-service');

    server = http.createServer(app);
    server.close = promisify(server.close);

    app.use(bodyParser());

    router(app, logger);

    server.listen(PORT, HOSTNAME, () => console.log(`${APPNAME} STARTED on ${HOSTNAME}:${PORT}`));

    // MongoDB Connection
    connectMongo(config, logger);

};

initServer();

// Graceful shutdown

const shutdown = type => {
    logger.warn(`Receive ${type} signal`);

    mongoose.disconnect();

    if (eureka) {
        Promise.all([
                eureka.stop().then(() => logger.warn('Eureka deregistred')),
                server.close().then(() => logger.warn('Server closed'))
            ])
            .then(() => {
                logger.warn('graceful EXIT');
                process.exit();
            })
            .catch(err => logger.error(err));
    } else {
        server.close().then(() => {
            logger.warn('Server closed');
            logger.warn('graceful EXIT');
            process.exit();
        });
    }
};

process.on('SIGINT', () => shutdown('SIGINT'));
process.on('SIGTERM', () => shutdown('SIGTERM'));