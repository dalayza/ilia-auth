import { signin, login, updatePassword, updateAccount, recoverPassword, validatePasswordResetToken, resetPassword } from './controllers/account';
import ErrorResponse from './response/error-response';
import DefaultResponse from './response/default-response';

export default (app, logger) => {
    app.get('/actuator/health', (req, res) => res.send({ status: 'UP' }));
    app.get('/actuator/info', (req, res) => res.send({}));

    app.get('/', (req, res) => res.status(200).send('Hello, Auth Service!!'));

    app.post('/signin', (req, res) => signin(req.body, logger)
        .then(ret => res.status(200).send(ret))
        .catch(err => {
            logger.error(err);
            res.status(400).send(new ErrorResponse(err));
        })
    );

    app.post('/login', (req, res) => login(req, res, logger)
        .then(ret => res.status(200).send(ret))
        .catch(err => {
            logger.error(err);
            res.send(new ErrorResponse(err));
        })
    );

    app.post('/updatePassword', (req, res) => updatePassword(req, res, logger)
        .then(ret => res.status(200).send(ret))
        .catch(err => {
            logger.error(err);
            res.status(400).send(new ErrorResponse(err));
        })
    );

    app.post('/updateAccount', (req, res) => updateAccount(req, res, logger)
        .then(ret => res.status(200).send(ret))
        .catch(err => {
            logger.error(err);
            res.status(400).send(new ErrorResponse(err));
        })
    );

    app.post('/recoverPassword', (req, res) => recoverPassword(req, logger)
        .then(() => res.status(200).send(new DefaultResponse({ resource: 'recoverPassword', data: {} })))
        .catch(message => {
            logger.error(message);
            res.status(400).send(new ErrorResponse(message));
        })
    );

    app.post('/validateToken', (req, res) => validatePasswordResetToken(req, logger)
        .then(ret => res.status(200).send(ret))
        .catch(message => {
            logger.error(message);
            res.status(400).send(new ErrorResponse(message));
        })
    );

    app.post('/resetPassword', (req, res) => resetPassword(req, logger)
        .then(ret => res.status(200).send(ret))
        .catch(message => {
            logger.error(message);
            res.status(400).send(new ErrorResponse(message));
        })
    );

};