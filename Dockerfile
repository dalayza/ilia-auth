FROM node:10

WORKDIR /usr/src/app

# Install PM2
RUN npm install -g pm2

COPY package*.json ./

RUN npm install
RUN mkdir log

COPY . .

# Expose port
EXPOSE 11002

# Run app
CMD ["pm2-runtime", "process.json"]
