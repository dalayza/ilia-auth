// Babel hook
require('@babel/polyfill');
require('@babel/register'); // eslint-disable-line import/no-extraneous-dependencies

// App
require('./src/app');
